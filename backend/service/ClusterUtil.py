import matplotlib.pyplot as plt
from matplotlib import style
from sklearn.cluster import KMeans
import numpy as np
import math
from kneed import KneeLocator


def get_k_means_all_features(points, percentageVariance):
    x, y, distance = zip(*points)

    x = list(x)
    y = list(y)
    distance = list(distance)

    cost = []
    labels = []
    centroids = []

    k = 1
    percentage = 100.0
    KM = KMeans(n_clusters=1, max_iter=2500, n_init=15, random_state=42)
    KM.fit(np.array(distance))
    labels.append(KM.labels_)
    centroids.append(KM.cluster_centers_)
    cost.append(KM.inertia_)
    while percentage > float(percentageVariance) and k < len(distance):
        k += 1
        KM = KMeans(n_clusters=k, max_iter=2500, n_init=15, random_state=42)
        KM.fit(np.array(distance))
        labels.append(KM.labels_)
        centroids.append(KM.cluster_centers_)
        cost.append(KM.inertia_)
        delta = abs(cost[k - 2] - cost[k - 1])
        percentage = (delta * 100.0) / cost[k - 2]
    knn = k - 1

    main_cluster, central_point = get_best_centroid_all_features(zip(x, y, distance, labels[knn-1]), centroids[knn-1])
    return zip(x, y, distance, labels[knn - 1]), knn, central_point, main_cluster


def get_best_centroid_all_features(dist, centroids):
    count = [0] * len(centroids)
    separated = [[] for i in range(len(centroids))]
    for element in dist:
        count[element[3]] += 1
        separated[element[3]].append(element)
    max = 0
    index = 0
    for i in range(len(count)):
        if count[i] > max:
            max = count[i]
            index = i
    min = float('inf')
    for element in separated[index]:
        sp_dist = 0
        indexxx = 0
        for band_value in element[2]:
            sp_dist += math.pow(band_value - centroids[index][indexxx], 2)
            indexxx += 1
        sp_dist = math.sqrt(sp_dist)
        if sp_dist < min:
            min = sp_dist
            closest_to_centroid = element
    return separated[index], closest_to_centroid

