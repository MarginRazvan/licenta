import cv2 as cv2
from ClusterStrategy import Context, LocalAreaCluster, ExtendedLocalAreaCluster
from SpectralRegionExtStrategy import ContextSp, PolygonalSpectralArea, SeedPropagation
import transform_geo


class Service:
    def __init__(self):
        path = '/home/razvan/an4/maps_licenta/cropped/B'
        self.images = []
        for sp_band in range(2, 13):
            if sp_band != 9 and sp_band != 10:
                self.images.append(cv2.imread(path + str(sp_band) + '.tiff', cv2.IMREAD_LOAD_GDAL))

    def lac(self, pointDTO, percentageVariance, length, width):
        x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/TCI.tiff",
                                             pointDTO.latitude,
                                             pointDTO.longitude)
        if x < 0 or y < 0:
            return []

        lac_strategy = LocalAreaCluster()
        context = Context(lac_strategy)
        imageTCI = cv2.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv2.IMREAD_LOAD_GDAL)
        return context.context_interface(x, y, imageTCI, self.images, percentageVariance, length, width)

    def elac(self, pointDTO, percentageVariance, length, width):
        x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/TCI.tiff",
                                             pointDTO.latitude,
                                             pointDTO.longitude)

        if x < 0 or y < 0:
            return []

        ext_strategy = ExtendedLocalAreaCluster()
        context = Context(ext_strategy)
        imageTCI = cv2.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv2.IMREAD_LOAD_GDAL)
        return context.context_interface(x, y, imageTCI, self.images, percentageVariance, length, width)

    def seed_propag(self, pointDTO, threshold):
        x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/TCI.tiff",
                                             pointDTO.latitude,
                                             pointDTO.longitude)
        if x < 0 or y < 0:
            return []
        seed_stategy = SeedPropagation()
        context = ContextSp(seed_stategy)
        imageTCI = cv2.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv2.IMREAD_LOAD_GDAL)
        return context.context_interface(x, y, imageTCI, self.images, threshold, 20, 20)

    def pol_spec_area(self, pointDTO, threshold, length, width):
        x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/TCI.tiff",
                                             pointDTO.latitude,
                                             pointDTO.longitude)
        if x < 0 or y < 0:
            return []
        pol_stategy = PolygonalSpectralArea()
        context = ContextSp(pol_stategy)
        imageTCI = cv2.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv2.IMREAD_LOAD_GDAL)
        return context.context_interface(x, y, imageTCI, self.images, threshold, length, width)
