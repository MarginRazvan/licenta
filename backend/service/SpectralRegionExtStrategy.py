import abc
import rasterio
import utm
import math
import xlwt

def write_to_excel(excel_points, x, y, images):
    wb = xlwt.Workbook()
    ws = wb.add_sheet('A Test Sheet')
    ws.write(0, 0, "Point")
    ws.write(0, 1, "B2")
    ws.write(0, 2, "B3")
    ws.write(0, 3, "B4")
    ws.write(0, 4, "B5")
    ws.write(0, 5, "B6")
    ws.write(0, 6, "B7")
    ws.write(0, 7, "B8")
    ws.write(0, 8, "B11")
    ws.write(0, 9, "B12")

    index = 0

    for element in excel_points:
        index += 1
        if index == 50:
            break
        im = images[0]
        ws.write(index, 0, "P" + str(index))
        indexx = 1
        for image in images:
            ws.write(index, indexx, float(image[element[0], element[1]] / 1000.0))
            indexx += 1
    wb.save('/home/razvan/an4/'+ str(x)+ '_'+ str(y) + '.xls')


class ContextSp:
    def __init__(self, strategy):
        self._strategy = strategy

    def context_interface(self, x, y, imageTCI, images, threshold, length, width):
        return self._strategy.spectral_region_algorithm(x, y, imageTCI, images, threshold, length, width)


class SpectralRegionExtStrategy():
    @abc.abstractmethod
    def spectral_region_algorithm(self, x, y, imageTCI, images, threshold, length, width):
        pass


class PolygonalSpectralArea(SpectralRegionExtStrategy):
    def spectral_region_algorithm(self, x, y, imageTCI, images, threshold, length, width):
        similar_points = []
        excel_points = []
        for i in range(x - int(width)/2, x + int(width)/2):
            for j in range(y - int(length)/2, y + int(length)/2):
                sp_dist = 0
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000.0) - (image[x, y] / 1000.0), 2)
                sp_dist = math.sqrt(sp_dist)

                if sp_dist <= threshold:
                    excel_points.append((i, j))
                    with rasterio.open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff') as map_layer:
                        pixels2coords = map_layer.xy(i, j)
                        lat_lon = utm.to_latlon(pixels2coords[0], pixels2coords[1], 34, 'T')
                        data = {}
                        data["latitude"] = lat_lon[0]
                        data["longitude"] = lat_lon[1]
                        similar_points.append(data)
        write_to_excel(excel_points, x, y, images)
        return similar_points


class SeedPropagation(SpectralRegionExtStrategy):
    def not_same_values(self, a, value1, value2, value3):
        if a[0] != value1 or a[1] != value2 or a[2] != value3:
            return True
        return False

    def spectral_region_algorithm(self, x, y, imageTCI, images, threshold, length, width):
        similar_points = []
        width = imageTCI.shape[0]
        height = imageTCI.shape[1]
        threshold = float(threshold)
        queue = [(x, y)]
        excel_points = []

        while len(queue) != 0:
            element = queue.pop(0)
            with rasterio.open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff') as map_layer:
                pixels2coords = map_layer.xy(element[0], element[1])
                lat_lon = utm.to_latlon(pixels2coords[0], pixels2coords[1], 34, 'T')
                data = {}
                data["latitude"] = lat_lon[0]
                data["longitude"] = lat_lon[1]
                similar_points.append(data)
            excel_points.append((element[0], element[1]))
            imageTCI[element[0], element[1]] = [0, 255, 0]
            for i in range(element[0] - 1, element[0] + 2):
                for j in range(element[1] - 1, element[1] + 2):
                    if (i != element[0] or j != element[1]) and i < width and j < height and self.not_same_values(
                            imageTCI[i, j], 0, 255, 0) and (not ((i, j) in queue)):
                        sp_dist = 0

                        for image in images:
                            sp_dist += math.pow((image[i, j] / 1000.0) - (image[x, y] / 1000.0), 2)
                        sp_dist = math.sqrt(sp_dist)
                        if sp_dist <= threshold:
                            queue.append((i, j))
        write_to_excel(excel_points, x, y, images)
        return similar_points
