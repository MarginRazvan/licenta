import abc
import rasterio
import utm
import xlwt
import ClusterUtil

def write_to_excel(excel_points, x, y, images):
    wb = xlwt.Workbook()
    ws = wb.add_sheet('A Test Sheet')
    ws.write(0, 0, "Point")
    ws.write(0, 1, "B2")
    ws.write(0, 2, "B3")
    ws.write(0, 3, "B4")
    ws.write(0, 4, "B5")
    ws.write(0, 5, "B6")
    ws.write(0, 6, "B7")
    ws.write(0, 7, "B8")
    ws.write(0, 8, "B11")
    ws.write(0, 9, "B12")
    index = 0
    for element in excel_points:
        index += 1
        if index == 50:
            break
        im = images[0]
        ws.write(index, 0, "P" + str(index))
        indexx = 1
        for image in images:
            ws.write(index, indexx, float(image[element[0], element[1]] / 1000.0))
            indexx += 1
    wb.save('/home/razvan/an4/'+ str(x)+ '_'+ str(y) + '.xls')


class Context:
    def __init__(self, strategy):
        self._strategy = strategy

    def context_interface(self, x, y, imageTCI, images, percentageVariance, length, width):
        return self._strategy.cluster_algorithm(x, y, imageTCI, images, percentageVariance, length, width)


class ClusterStrategy():
    @abc.abstractmethod
    def cluster_algorithm(self, x, y, imageTCI, images, percentageVariance, length, width):
        pass


class LocalAreaCluster(ClusterStrategy):
    def cluster_algorithm(self, x, y, imageTCI, images, percentageVariance, length, width):
        cluster_points = []
        excel_points = []

        # follow box
        for i in range(x - int(width)/2, x + int(width)/2):
            for j in range(y - int(length)/2, y + int(length)/2):
                t = []
                imageTCI[i, j] = [0, 255, 255]
                for image in images:
                    t.append(image[i, j] / 1000.0)
                cluster_points.append((i, j, t))
        clusters, labels, central_point, main_cluster = ClusterUtil.get_k_means_all_features(cluster_points, percentageVariance)
        main_cluster_converted = []
        for element in main_cluster:
            excel_points.append((element[0], element[1]))
            with rasterio.open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff') as map_layer:
                pixels2coords = map_layer.xy(element[0], element[1])
                lat_lon = utm.to_latlon(pixels2coords[0], pixels2coords[1], 34, 'T')
                data = {}
                data["latitude"] = lat_lon[0]
                data["longitude"] = lat_lon[1]
                main_cluster_converted.append(data)
        write_to_excel(excel_points, x, y, images)
        return main_cluster_converted


class ExtendedLocalAreaCluster(ClusterStrategy):
    def cluster_algorithm(self, x, y, imageTCI, images, percentageVariance, length, width):
        # follow box
        percentage_prev = 0.0
        initial_incr_down = int(width)/2
        initial_incr_up = int(width)/2
        initial_incr_left = int(length)/2
        initial_incr_right = int(length)/2
        percentage_now = 100.0
        flag = True
        index = 0
        while percentage_now > 60.0 and flag is True:
            index += 1
            cluster_points = []
            for i in range(x - initial_incr_up, x + initial_incr_down):
                for j in range(y - initial_incr_left, y + initial_incr_right):
                    sp_dist = 0
                    t = []
                    for image in images:
                        t.append(image[i, j] / 1000.0)
                        sp_dist += image[i, j] / 1000.0
                    cluster_points.append((i, j, t))
            clusters, labels, central_point, main_cluster = ClusterUtil.get_k_means_all_features(cluster_points, percentageVariance)

            percentage_now = 100.0 * len(main_cluster) / len(cluster_points)
            flag = True

            boundaries = [0, 0, 0, 0]
            for item in main_cluster:
                if item[1] == y - initial_incr_left:
                    boundaries[0] += 1
                if item[1] == y - initial_incr_right - 1:
                    boundaries[1] += 1
                if item[0] == x - initial_incr_up:
                    boundaries[2] += 1
                if item[0] == x - initial_incr_down - 1:
                    boundaries[3] += 1
            maximum = 0
            index_max = -1
            for index in range(0, 4):
                if boundaries[index] > maximum:
                    maximum = boundaries[index]
                    index_max = index

            if index_max == 0:
                initial_incr_left += 1
            elif index_max == 1:
                initial_incr_right += 1
            elif index_max == 2:
                initial_incr_up += 1
            elif index_max == 3:
                initial_incr_down += 1
            else:
                flag = False

        excel_points =[]
        main_cluster_converted = []
        for element in main_cluster:
            excel_points.append((element[0], element[1]))
            with rasterio.open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff') as map_layer:
                pixels2coords = map_layer.xy(element[0], element[1])
                lat_lon = utm.to_latlon(pixels2coords[0], pixels2coords[1], 34, 'T')
                data = {}
                data["latitude"] = lat_lon[0]
                data["longitude"] = lat_lon[1]
                main_cluster_converted.append(data)
        write_to_excel(excel_points, x, y, images)
        return main_cluster_converted
