import cv2
import math
point = (-1, -1)
shown = False

path = '/home/razvan/an4/maps_licenta/cropped/B'
images = []

# Note to self -> separate difference of bands


def process_image(x, y, epsilon, color_diff):
    width = image_tci_initial.shape[0]
    height = image_tci_initial.shape[1]
    epsilon = float(epsilon) / 10
    queue = [(x, y)]
    value_initial = image_tci_initial[x, y]

    while len(queue) != 0:
        element = queue.pop(0)
        image_tci_after[element[0], element[1]] = [0, 255, 0]
        for i in range(element[0] - 1, element[0] + 2):
            for j in range(element[1] - 1, element[1] + 2):
                if (i != element[0] or j != element[1]) and i < width and j < height and not_same_values(image_tci_after[i, j], 0, 255, 0) and (not ((i, j) in queue)):
                    sp_dist = 0
                    color_dist = abs(int(image_tci_initial[i, j][0]) - int(value_initial[0]))
                    color_dist += abs(int(image_tci_initial[i, j][1]) - int(value_initial[1]))
                    color_dist += abs(int(image_tci_initial[i, j][2]) - int(value_initial[2]))

                    for image in images:
                        sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                    if sp_dist <= epsilon and color_dist <= color_diff:
                        queue.append((i, j))


def not_same_values(a, value1, value2, value3):
    if a[0] != value1 or a[1] != value2 or a[2] != value3:
        return True
    return False


def click(event, y, x, flags, param):
    global point, images, shown
    # if left mouse is clicked, record coordinates (x,y)
    if event == cv2.EVENT_LBUTTONDOWN:
        point = (x, y)
        shown = False

def nothing(x):
    pass



for ii in range(2, 13):
    if ii != 9 and ii != 10:
        images.append(cv2.imread(path + str(ii) + '.tiff', cv2.IMREAD_LOAD_GDAL))


image_tci_initial = cv2.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv2.IMREAD_LOAD_GDAL)
cv2.imshow('Iclod_Initial', image_tci_initial)

image_tci_after = image_tci_initial.copy()

cv2.namedWindow("Iclod_process")
cv2.setMouseCallback("Iclod_process", click)
cv2.createTrackbar("trackbar", "Iclod_process", 0, 50, nothing)
cv2.createTrackbar("trackbar2", "Iclod_process", 0, 50, nothing)
cv2.imshow("Iclod_process", image_tci_after)


# keep looping until q key is pressed
while True:
    cv2.imshow("Iclod_process", image_tci_after)
    key = cv2.waitKey(500) & 0xFF

    if key == ord("r"):
        image_tci_after = image_tci_initial.copy()

    if key == ord("q"):
        break

    epsilonn = cv2.getTrackbarPos("trackbar", "Iclod_process")
    color_difff = cv2.getTrackbarPos("trackbar2", "Iclod_process")

    if point[0] != -1:
        image_tci_after = image_tci_initial.copy()
        process_image(point[0], point[1], epsilonn, color_difff)


cv2.waitKey(0)
cv2.destroyAllWindows()