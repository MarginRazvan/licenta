import cv2
import math
import xlwt
import sys
print sys.path
sys.path.append('/usr/lib/python2.7/dist-packages/')
from osgeo import gdal
from osgeo import osr
point = (-1, -1)
shown = False
set = False
import Cluster

path = '/home/razvan/an4/maps_licenta/cropped/B'
images = []
excel_points = []

def write_to_excel():
    global excel_points, images
    print excel_points

    img_coord = gdal.Open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff')

    xoffset, px_w, rot1, yoffset, px_h, rot2 = img_coord.GetGeoTransform()

    # get CRS from dataset
    crs = osr.SpatialReference()
    crs.ImportFromWkt(img_coord.GetProjectionRef())
    # create lat/long crs with WGS84 datum
    crsGeo = osr.SpatialReference()
    crsGeo.ImportFromEPSG(4326)  # 4326 is the EPSG id of lat/long crs
    t = osr.CoordinateTransformation(crs, crsGeo)

    wb = xlwt.Workbook()
    ws = wb.add_sheet('A Test Sheet')
    #
    ws.write(0, 0, "Point")
    ws.write(0, 1, "Latitude")
    ws.write(0, 2, "Longitude")
    ws.write(0, 3, "B2")
    ws.write(0, 4, "B3")
    ws.write(0, 5, "B4")
    ws.write(0, 6, "B5")
    ws.write(0, 7, "B6")
    ws.write(0, 8, "B7")
    ws.write(0, 9, "B8")
    ws.write(0, 10, "B11")
    ws.write(0, 11, "B12")

    index = 0
    # print excel_points

    for element in excel_points:
        index += 1
        if index == 50:
            break

        im = images[0]
        ws.write(index, 0, "P" + str(index))

        posX = px_w * element[0] + rot1 * element[1] + xoffset
        posY = rot2 * element[0] + px_h * element[1] + yoffset

        posX += px_w / 2.0
        posY += px_h / 2.0
        (longitude, latitude, z) = t.TransformPoint(posX, posY)
        print "[ " + str(element[0]) + "," + str(element[1]) + " ] => " + str(latitude) + " " + str(longitude) + "\n"

        ws.write(index, 1, latitude)
        ws.write(index, 2, longitude)

        indexx = 3
        for image in images:
            ws.write(index, indexx, float(image[element[0], element[1]] / 10000.0))
            indexx += 1
    print "done"
    wb.save('/home/razvan/an4/licenta_analysis/data/'+ str(point[0])+ '_'+ str(point[1]) + '.xls')

def get_best_point(x, y):
    cluster_points = []
    for i in range(x - 10, x + 10):
        for j in range(y - 10, y + 10):
            sp_dist = 0
            for image in images:
                sp_dist += math.pow((image[i, j] / 1000.0) - (image[x, y] / 1000.0), 2)
            sp_dist = math.sqrt(sp_dist)
            cluster_points.append((i, j, sp_dist))
    cluster_processed, labels, central_point = Cluster.get_k_means(cluster_points)
    return central_point


def process_image(x, y, epsilon, color_diff):
    global excel_points
    excel_points = []
    cluster_points = []
    width = image_tci_initial.shape[0]
    height = image_tci_initial.shape[1]
    epsilon = float(epsilon) * 0.05

    queue = [(x, y)]
    value_initial = image_tci_initial[x, y]

    while len(queue) != 0:
        element = queue.pop(0)
        excel_points.append((element[0], element[1]))
        image_tci_after[element[0], element[1]] = [0, 255, 0]
        for i in range(element[0] - 1, element[0] + 2):
            for j in range(element[1] - 1, element[1] + 2):
                if (i != element[0] or j != element[1]) and i < width and j < height and not_same_values(image_tci_after[i, j], 0, 255, 0) and (not ((i, j) in queue)):
                    sp_dist = 0

                    for image in images:
                        sp_dist += math.pow((image[i, j] / 1000.0) - (image[x, y] / 1000.0), 2)
                    sp_dist = math.sqrt(sp_dist)

                    if sp_dist <= epsilon:
                        print sp_dist
                        queue.append((i, j))

def not_same_values(a, value1, value2, value3):
    if a[0] != value1 or a[1] != value2 or a[2] != value3:
        return True
    return False


def click(event, y, x, flags, param):
    global point, images, shown
    # if left mouse is clicked, record coordinates (x,y)
    if event == cv2.EVENT_LBUTTONDOWN:
        point = (x, y)
        shown = False

def nothing(x):
    pass

for ii in range(2, 13):
    if ii != 9 and ii != 10:
        images.append(cv2.imread(path + str(ii) + '.tiff', cv2.IMREAD_LOAD_GDAL))


image_tci_initial = cv2.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv2.IMREAD_LOAD_GDAL)

cv2.imshow('Iclod_Initial', image_tci_initial)

image_tci_after = image_tci_initial.copy()

cv2.namedWindow("Iclod_process")
cv2.setMouseCallback("Iclod_process", click)
cv2.createTrackbar("epsilon", "Iclod_process", 0, 20, nothing)
# cv2.createTrackbar("color_dif", "Iclod_process", 0, 50, nothing)
cv2.imshow("Iclod_process", image_tci_after)


# keep looping until q key is pressed
while True:
    cv2.imshow("Iclod_process", image_tci_after)
    key = cv2.waitKey(500) & 0xFF

    if key == ord("r"):
        image_tci_after = image_tci_initial.copy()
        set = False

    if key == ord("q"):
        break
    if key == ord("s"):
        write_to_excel()
        cv2.imwrite("/home/razvan/an4/licenta_analysis/data/try.tiff", image_tci_after)

    if key == ord("c"):
        point = get_best_point(point[0], point[1])
        set = True


    if set == True:
        epsilonn = cv2.getTrackbarPos("epsilon", "Iclod_process")
        image_tci_after = image_tci_initial.copy()
        process_image(point[0], point[1], epsilonn, 0)


cv2.waitKey(0)
cv2.destroyAllWindows()