import sys
sys.path.append('/usr/lib/python2.7/dist-packages/')
from osgeo import gdal
from osgeo import osr
import affine
import numpy as np
import rasterio
import utm
import cv2


def get_coord_pixel(filename, lat, lon):
    utm_lat, utm_lon, _, _ = utm.from_latlon(lat, lon)
    driver = gdal.GetDriverByName('GTiff')
    # filename = "/home/razvan/an4/maps_licenta/cropped/TCI.tiff"
    dataset = gdal.Open(filename)
    band = dataset.GetRasterBand(1)

    cols = dataset.RasterXSize
    rows = dataset.RasterYSize

    transform = dataset.GetGeoTransform()

    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = -transform[5]

    data = band.ReadAsArray(0, 0, cols, rows)

    # points_list = [(utm_lat, utm_lon)]

    col = int((utm_lat - xOrigin) / pixelWidth)
    row = int((yOrigin - utm_lon) / pixelHeight)

    return row, col

def get_coord_pixel_utm(filename, utm_lat, utm_lon):
    driver = gdal.GetDriverByName('GTiff')
    # filename = "/home/razvan/an4/maps_licenta/cropped/TCI.tiff"
    dataset = gdal.Open(filename)
    band = dataset.GetRasterBand(1)

    cols = dataset.RasterXSize
    rows = dataset.RasterYSize

    transform = dataset.GetGeoTransform()

    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = -transform[5]

    data = band.ReadAsArray(0, 0, cols, rows)

    # points_list = [(utm_lat, utm_lon)]

    col = int((utm_lat - xOrigin) / pixelWidth)
    row = int((yOrigin - utm_lon) / pixelHeight)

    return row, col
    # for point in points_list:
    # for point in points_list:
    #     col = int((point[0] - xOrigin) / pixelWidth)
    #     row = int((yOrigin - point[1] ) / pixelHeight)
    #     image_initial[row, col] = [0, 255, 0]
    # print row, col, data[row][col]


with rasterio.open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff') as map_layer:
            pixels2coords = map_layer.xy(130, 223)
            aaa = utm.to_latlon(pixels2coords[0], pixels2coords[1], 34, 'T')
            print aaa
