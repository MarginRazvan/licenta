import cv2 as cv
import math
path = '/home/razvan/an4/maps_licenta/cropped/B'
images = []


def not_same_values(a):
    if a[0] != 0 or a[1] != 0 or a[2] != 255:
        return True
    return False

# Load images
for i in range(2,13):
    if i != 9 and i != 10:
        images.append(cv.imread(path + str(i) + '.tiff', cv.IMREAD_LOAD_GDAL))

image_tci = cv.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv.IMREAD_LOAD_GDAL)
image_tci2 = cv.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv.IMREAD_LOAD_GDAL)
image_tci3 = cv.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv.IMREAD_LOAD_GDAL)
image_tci4 = cv.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv.IMREAD_LOAD_GDAL)

height = image_tci.shape[0]
width = image_tci.shape[1]

# y inversat cu x
y = 191
x = 146
print x
print y
cv.imshow('Iclod_Initial', image_tci)

# BGR
# compute spectral distance as sqrt

# follow box
for i in range(x-10, x+10):
    for j in range(y-10, y+10):
        sp_dist = 0
        for image in images:
            sp_dist += math.pow((image[i, j] / 1000.0) - (image[x, y]/1000.0), 2)
        sp_dist = math.sqrt(sp_dist)
        # print str(i) + ' ' + str(j) + '=>' + str(sp_dist)
        if sp_dist <= 1.8:
            image_tci[i, j] = [0, 0, 255]
        # else:
        #     image_tci[i, j] = [0, 255, 255]

image_tci[x, y] = [0, 0, 255]



cv.imshow('Iclod_after_box', image_tci)
print "done"



# follow neighbors
# epsilon 3.0 or 2.5
queue = [(x, y)]
index = 0
while len(queue) != 0:
    element = queue.pop(0)
    # print queue
    image_tci2[element[0], element[1]] = [0, 0, 255]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):
            if (i != element[0] or j != element[1]) and not_same_values(image_tci2[i, j]) and (not ((i,j) in queue)):
                sp_dist = 0
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                if sp_dist <= 3.5:
                    queue.append((i, j))

cv.imshow('Iclod_after_neigh', image_tci2)

# Only band 11 => soils detection
print "band 11"
images = []
images.append(cv.imread(path + '11.tiff', cv.IMREAD_LOAD_GDAL))

# follow neighbors
queue = [(x, y)]
index = 0
while len(queue) != 0:
    element = queue.pop(0)
    # print queue
    image_tci3[element[0], element[1]] = [0, 0, 255]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):

            if (i != element[0] or j != element[1]) and not_same_values(image_tci3[i, j]) and (not ((i,j) in queue)):
                sp_dist = 0
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                # print str(i) + ' ' + str(j) + '=>' + str(sp_dist)
                if sp_dist >= 0.5:
                    queue.append((i, j))

cv.imshow('Iclod_after_neigh_BAND11', image_tci3)

# Band 2 3 4 8 11
print "band 2 3 4 8 11"
images = []
images.append(cv.imread(path + '2.tiff', cv.IMREAD_LOAD_GDAL))
images.append(cv.imread(path + '3.tiff', cv.IMREAD_LOAD_GDAL))
images.append(cv.imread(path + '4.tiff', cv.IMREAD_LOAD_GDAL))
images.append(cv.imread(path + '8.tiff', cv.IMREAD_LOAD_GDAL))
images.append(cv.imread(path + '11.tiff', cv.IMREAD_LOAD_GDAL))

# follow neighbors
queue = [(x, y)]
index = 0
while len(queue) != 0:
    element = queue.pop(0)
    # print queue
    image_tci4[element[0], element[1]] = [0, 0, 255]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):

            if (i != element[0] or j != element[1]) and not_same_values(image_tci4[i, j]) and (not ((i,j) in queue)):
                sp_dist = 0
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                print str(i) + ' ' + str(j) + '=>' + str(sp_dist)
                if sp_dist <= 1.0:
                    queue.append((i, j))

cv.imshow('Iclod_after_neigh_BAND2_3_4_8_11', image_tci4)






cv.waitKey(0)
cv.destroyAllWindows()



