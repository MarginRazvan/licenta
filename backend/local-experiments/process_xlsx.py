import openpyxl
import Cluster
from openpyxl.styles import Font
import cv2
from random import randint
import transform_geo
import sys
sys.path.append('/usr/lib/python2.7/dist-packages/')


def cluster_all(points):
    print "Final clustering"


def iter_rows(ws, n):  # produce the list of items in the particular row
    for row in ws.iter_rows(n):
        yield [cell.value for cell in row]

image_initial = cv2.imread( '/home/razvan/an4/maps_licenta/cropped/sic.tiff' , cv2.IMREAD_LOAD_GDAL)
image_after = image_initial.copy()
book = openpyxl.load_workbook('/home/razvan/an4/licenta_analysis/filtered/more_data/combined_sat.xlsx')
book_processed = openpyxl.Workbook()
final_list = []
for sheet in book.worksheets:
    print sheet.title
    cluster_points = []

    values = sheet['K2':'K101']
    indexxx = 2
    for item in values:
        for value in item:
            if value.value is None:
                break
            indexxx+=1
    # print indexxx

    bands_values = sheet['K2': 'V'+str(indexxx-1)]
    index = 2
    # verifica daca ii None o celula
    for item in bands_values:
        t = []
        bands_index = 0
        for band_value in item:
            bands_index += 1
            if band_value.value == None:
                break
            # if bands_index == 3 or bands_index == 4 or bands_index == 5 or bands_index == 8 or bands_index == 10 or bands_index == 11 or bands_index == 12:
            t.append(band_value.value)
        cluster_points.append((index, (sheet['E'+str(index)].value, sheet['F'+str(index)].value), t))
        index += 1
    sheet_processed = book_processed.create_sheet(title=sheet.title)
    if len(cluster_points) > 4:
        clusters, knee, central_point, main_cluster = Cluster.get_k_means_all_features(cluster_points)
        for el in main_cluster:
            final_list.append((el[0], el[1], el[2]))
        colors = []
        for i in range(0, knee):
            colors.append([randint(0, 255), randint(0, 255), randint(0, 255)])
        # print colors
        # print labels
        for o in clusters:
            x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/sic.tiff", o[1][0], o[1][1])
            if x >= image_after.shape[0] or y >= image_after.shape[1]:
                pass
            else:
                image_after[x, y] = colors[o[3]]
        x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/sic.tiff", central_point[1][0], central_point[1][1])
        if x >= image_after.shape[0] or y >= image_after.shape[1]:
            pass
        else:
            image_after[x, y] = [0, 255, 0]


        counter = 0
        new_rows =[]

        header = 1
        # append head of table
        for row in sheet['A'+str(header) : 'IU'+str(header)]:
            new_rows.append([])
            for cell in row:
                new_rows[counter].append(cell.value)
            counter += 1

        x, _, _, _= zip(*main_cluster)
        coloured_index = 0
        x = list(x)
        red_font = Font(color='00FF0000', italic=True)
        for i in x:
            # print str(i)+ ' '+str(central_point[0])
            if i == central_point[0]:
                coloured_index = counter + 1
                # print coloured_index
            for row in sheet['A' + str(i): 'IU' + str(i)]:
                new_rows.append([])
                for cell in row:
                    new_rows[counter].append(cell.value)
                counter += 1

        index = 0
        for wrow in new_rows:
            sheet_processed.append(wrow)
        for row in sheet_processed['A'+str(coloured_index): 'IU' + str(coloured_index)]:
            for cell in row:
                cell.font = red_font


book_processed.save("/home/razvan/an4/licenta_analysis/filtered/more_data/csv3_6%.xlsx")
print "Saved"
cv2.imshow('sic_after', image_after)

# clusters, knee, central_point, main_cluster = Cluster.get_k_means_all_features(final_list, True, 30)
# print len(final_list)
# colors = []
# for i in range(0, knee):
#     colors.append([randint(0, 255), randint(0, 255), randint(0, 255)])
# # print colors
# # print labels
# for o in clusters:
#     x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/sic.tiff", o[1][0], o[1][1])
#     if x > image_initial.shape[0] or y > image_initial.shape[1]:
#         pass
#     else:
#         image_initial[x, y] = colors[o[3]]
# x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/sic.tiff", central_point[1][0], central_point[1][1])
# if x > image_initial.shape[0] or y > image_initial.shape[1]:
#     pass
# else:
#     image_initial[x, y] = [0, 255, 0]
# cv2.imshow('sic_Initial', image_initial)
# cv2.imwrite('/home/razvan/an4/maps_licenta/cropped/sic_clusters_2.tiff', image_initial)
# src = '/home/razvan/an4/maps_licenta/cropped/sic_clusters_2.tiff'
# gdal_translate -of GTiff -a_srs "EPSG:32634" -a_ullr 716020.000 5205700.000 725360.000 5196940.000 /home/razvan/an4/maps_licenta/cropped/sic_clusters_2.tiff /home/razvan/an4/maps_licenta/cropped/sic_clusters_2_gdallll.tiff



cv2.waitKey(0)
cv2.destroyAllWindows()


# clustere oprire sub k epsilon
#
#

