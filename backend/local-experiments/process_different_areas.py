import cv2 as cv
import math
path = '/home/razvan/an4/maps_licenta/cropped/B'
images = []


def not_same_values(a, value1, value2, value3):
    if a[0] != value1 or a[1] != value2 or a[2] != value3:
        return True
    return False

# Load images


for i in range(2, 13):
    if i != 9 and i != 10:
        images.append(cv.imread(path + str(i) + '.tiff', cv.IMREAD_LOAD_GDAL))

image_tci = cv.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv.IMREAD_LOAD_GDAL)

height = image_tci.shape[0]
width = image_tci.shape[1]

# y inversat cu x
y = 191
x = 146
print x
print y
cv.imshow('Iclod_Initial', image_tci)

# follow neighbors
# epsilon 3.0 or 2.5
queue = [(x, y)]
while len(queue) != 0:
    element = queue.pop(0)
    image_tci[element[0], element[1]] = [0, 0, 255]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):
            if (i != element[0] or j != element[1]) and not_same_values(image_tci[i, j], 0, 0, 255) and (not ((i,j) in queue)):
                sp_dist = 0
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                if sp_dist <= 2.5:
                    queue.append((i, j))


y = 222
x = 129

# follow neighbors
# epsilon 3.0 or 2.5
queue = [(x, y)]
while len(queue) != 0:
    element = queue.pop(0)
    image_tci[element[0], element[1]] = [255, 0, 0]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):
            if (i != element[0] or j != element[1]) and not_same_values(image_tci[i, j], 255, 0, 0) and (not ((i,j) in queue)):
                sp_dist = 0
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                if sp_dist <= 2.5:
                    queue.append((i, j))

y = 127
x = 109

# follow neighbors
# epsilon 3.0 or 2.5
queue = [(x, y)]
while len(queue) != 0:
    element = queue.pop(0)
    image_tci[element[0], element[1]] = [0, 255, 0]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):
            if (i != element[0] or j != element[1]) and not_same_values(image_tci[i, j], 0, 255, 0) and (not ((i,j) in queue)):
                sp_dist = 0
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                if sp_dist <= 0.5:
                    queue.append((i, j))


cv.imshow('Iclod_after_neigh', image_tci)
cv.waitKey(0)
cv.destroyAllWindows()