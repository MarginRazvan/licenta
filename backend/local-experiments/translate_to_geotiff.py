import os

folderPath = '/home/razvan/an4/maps_licenta'
folderItems = os.listdir(folderPath)

conversionFolderPath = os.path.join(folderPath, "Transilvania")

if not os.path.exists(conversionFolderPath):
    os.makedirs(conversionFolderPath)

for item in folderItems:
    itemFullPath = os.path.join(folderPath, item)
    if os.path.isfile(itemFullPath):
        if item.endswith('.jp2'):

            fileNameWithoutOriginalExtansion = item[:-3]

            destinationFile = os.path.join(conversionFolderPath, fileNameWithoutOriginalExtansion + '.gtiff')
            os.system("gdal_translate -of GTiff " + itemFullPath + " " +  destinationFile)
