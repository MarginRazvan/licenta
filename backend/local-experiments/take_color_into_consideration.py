import cv2 as cv
import math
path = '/home/razvan/an4/maps_licenta/cropped/B'
images = []


def not_same_values(a, value1, value2, value3):
    if a[0] != value1 or a[1] != value2 or a[2] != value3:
        return True
    return False

# Load images


image_tci_initial = cv.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv.IMREAD_LOAD_GDAL)
image_tci_after = cv.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv.IMREAD_LOAD_GDAL)

for i in range(2, 13):
    if i != 9 and i != 10:
        images.append(cv.imread(path + str(i) + '.tiff', cv.IMREAD_LOAD_GDAL))

cv.imshow('Iclod_Initial', image_tci_initial)

y = 127
x = 109
# follow neighbors
# epsilon 3.0 or 2.5
queue = [(x, y)]
value_initial = image_tci_initial[x, y]

while len(queue) != 0:
    element = queue.pop(0)
    image_tci_after[element[0], element[1]] = [0, 255, 0]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):
            if (i != element[0] or j != element[1]) and not_same_values(image_tci_after[i, j], 0, 255, 0) and (not ((i,j) in queue)):
                sp_dist = 0
                print str(image_tci_initial[i,j]) + " " + str(value_initial)
                color_dist = abs(int(image_tci_initial[i, j][0]) - int(value_initial[0]))
                color_dist += abs(int(image_tci_initial[i, j][1]) - int(value_initial[1]))
                color_dist += abs(int(image_tci_initial[i, j][2]) - int(value_initial[2]))

                print color_dist
                # print "color => " + str(color_dist)
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                if sp_dist <= 3.5 and color_dist <= 15:
                    queue.append((i, j))


y = 224
x = 131
# follow neighbors
# epsilon 3.0 or 2.5
queue = [(x, y)]
value_initial = image_tci_initial[x, y]

while len(queue) != 0:
    element = queue.pop(0)
    image_tci_after[element[0], element[1]] = [0, 255, 0]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):
            if (i != element[0] or j != element[1]) and not_same_values(image_tci_after[i, j], 0, 255, 0) and (not ((i,j) in queue)):
                sp_dist = 0
                print str(image_tci_initial[i,j]) + " " + str(value_initial)
                color_dist = abs(int(image_tci_initial[i, j][0]) - int(value_initial[0]))
                color_dist += abs(int(image_tci_initial[i, j][1]) - int(value_initial[1]))
                color_dist += abs(int(image_tci_initial[i, j][2]) - int(value_initial[2]))

                print color_dist
                # print "color => " + str(color_dist)
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                if sp_dist <= 3.5 and color_dist <= 15:
                    queue.append((i, j))

y = 262
x = 158
# follow neighbors
# epsilon 3.0 or 2.5
queue = [(x, y)]
value_initial = image_tci_initial[x, y]

while len(queue) != 0:
    element = queue.pop(0)
    image_tci_after[element[0], element[1]] = [0, 255, 0]
    for i in range(element[0]-1, element[0]+2):
        for j in range(element[1]-1, element[1]+2):
            if (i != element[0] or j != element[1]) and not_same_values(image_tci_after[i, j], 0, 255, 0) and (not ((i,j) in queue)):
                sp_dist = 0
                print str(image_tci_initial[i,j]) + " " + str(value_initial)
                color_dist = abs(int(image_tci_initial[i, j][0]) - int(value_initial[0]))
                color_dist += abs(int(image_tci_initial[i, j][1]) - int(value_initial[1]))
                color_dist += abs(int(image_tci_initial[i, j][2]) - int(value_initial[2]))

                print color_dist
                # print "color => " + str(color_dist)
                for image in images:
                    sp_dist += math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                if sp_dist <= 3.5 and color_dist <= 15:
                    queue.append((i, j))

cv.imshow('Iclod_after_neigh', image_tci_after)
cv.waitKey(0)
cv.destroyAllWindows()