import cv2
import math
import xlwt
import sys
print sys.path
sys.path.append('/usr/lib/python2.7/dist-packages/')
from osgeo import gdal
from osgeo import osr

point = (-1, -1)
shown = False
import Cluster
from random import randint
path = '/home/razvan/an4/maps_licenta/cropped/B'
images = []
excel_points = []
# Note to self -> separate difference of bands
# / 1000 does not work
def write_to_excel():
    global excel_points, images
    print excel_points
    wb = xlwt.Workbook()
    ws = wb.add_sheet('A Test Sheet')
    #
    ws.write(0, 0, "Point")
    ws.write(0, 1, "B2")
    ws.write(0, 2, "B3")
    ws.write(0, 3, "B4")
    ws.write(0, 4, "B5")
    ws.write(0, 5, "B6")
    ws.write(0, 6, "B7")
    ws.write(0, 7, "B8")
    ws.write(0, 8, "B11")
    ws.write(0, 9, "B12")

    index = 0
    # print excel_points

    for element in excel_points:

        index += 1
        if index == 50:
            break

        im = images[0]
        ws.write(index, 0, "P" + str(index))
        indexx = 1
        for image in images:
            ws.write(index, indexx, float(image[element[0], element[1]] / 10000.0))
            indexx += 1
    print "done"
    wb.save('/home/razvan/an4/'+ str(point[0])+ '_'+ str(point[1]) + '.xls')



def process_image(x, y, epsilon, color_diff):
    global excel_points
    excel_points = []
    width = image_tci_initial.shape[0]
    height = image_tci_initial.shape[1]
    epsilon = float(epsilon) / 10
    queue = [(x, y)]
    value_initial = image_tci_initial[x, y]

    while len(queue) != 0:
        element = queue.pop(0)
        excel_points.append((element[0], element[1]))
        image_tci_after[element[0], element[1]] = [0, 255, 0]
        for i in range(element[0] - 1, element[0] + 2):
            for j in range(element[1] - 1, element[1] + 2):
                if (i != element[0] or j != element[1]) and i < width and j < height and not_same_values(image_tci_after[i, j], 0, 255, 0) and (not ((i, j) in queue)):
                    sp_dist = 0

                    for image in images:
                        sp_dist += math.pow((image[i, j] / 1000.0) - (image[x, y] / 1000.0), 2)
                        # print math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
                    if sp_dist <= epsilon:
                        queue.append((i, j))


def process_image_static(x, y):
    # global excel_points
    # excel_points = []
    width = image_tci_initial.shape[0]
    height = image_tci_initial.shape[1]
    # epsilon = float(epsilon) / 10
    queue = [(x, y)]
    value_initial = image_tci_initial[x, y]

    cluster_points = []# contiguous clustering
    # follow box
    for i in range(x - 10, x + 10):
        for j in range(y - 10, y + 10):
            sp_dist = 0
            t = []
            for image in images:
                # t += (image[i, j]/1000.0,)
                t.append(image[i, j]/1000.0)
                # sp_dist += math.pow((image[i, j] / 1000.0) - (image[x, y] / 1000.0), 2)
                sp_dist += image[i, j] / 1000.0
            # sp_dist = math.sqrt(sp_dist)
            # print t
            # print str(i) + ' ' + str(j) + '=>' + str(sp_dist)
            cluster_points.append((i, j, t))
    # print cluster_points
    # cluster_processed, labels, central_point = Cluster.get_k_means_all_features(cluster_points)
    clusters, labels, central_point, main_cluster = Cluster.get_k_means_all_features(cluster_points)
    for el in main_cluster:
        excel_points.append(el)
    colors = []
    for i in range (0, labels):
        colors.append([randint(0,255), randint(0,255), randint(0,255)])
    # print colors
    # print labels
    for o in clusters:
        image_tci_after[o[0], o[1]] = colors[o[3]]
        # 23.7832623271
        # 47.0008622581

        # with rasterio.open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff') as map_layer:
        #     pixels2coords = map_layer.xy(o[0], o[1])
        #
        #     # xxx, yyy = utm.to_latlon(pixels2coords[0], pixels2coords[1], 34, 'T')
        #     xx, yy = transform_geo.get_coord_pixel_utm('/home/razvan/an4/maps_licenta/ICLOD_map1_EPSG_3857.tif', pixels2coords[0], pixels2coords[1])
        #     # xx, yy = transform_geo.get_coord_pixel('/home/razvan/an4/maps_licenta/ICLOD_map1_EPSG_3857.tif', 47.001362, 23.806363)
        #     # xx, yy = transform_geo.get_coord_pixel('/home/razvan/an4/maps_licenta/ICLOD_map1v2_rectified_WGS84.tif', 47.001362, 23.806363)
        # print "Str"


            # coords2pixels = map_layer.index()
    image_tci_after[central_point[0], central_point[1]] = [0, 255, 0]


            # if sp_dist <= 1.8:
            #     image_tci[i, j] = [0, 0, 255]
            # else:
            #     image_tci[i, j] = [0, 255, 255]

    # while len(queue) != 0:
    #     element = queue.pop(0)
    #     excel_points.append((element[0], element[1]))
    #     image_tci_after[element[0], element[1]] = [0, 255, 0]
    #     for i in range(element[0] - 1, element[0] + 2):
    #         for j in range(element[1] - 1, element[1] + 2):
    #             if (i != element[0] or j != element[1]) and i < width and j < height and not_same_values(image_tci_after[i, j], 0, 255, 0) and (not ((i, j) in queue)):
    #                 sp_dist = 0
    #                 # color_dist = abs(int(image_tci_initial[i, j][0]) - int(value_initial[0]))
    #                 # color_dist += abs(int(image_tci_initial[i, j][1]) - int(value_initial[1]))
    #                 # color_dist += abs(int(image_tci_initial[i, j][2]) - int(value_initial[2]))
    #
    #                 for image in images:
    #                     sp_dist += math.pow((image[i, j] / 1000.0) - (image[x, y] / 1000.0), 2)
    #                     # print math.pow((image[i, j] / 1000) - (image[x, y] / 1000), 2)
    #                 if sp_dist <= epsilon:
    #                     queue.append((i, j))

def not_same_values(a, value1, value2, value3):
    if a[0] != value1 or a[1] != value2 or a[2] != value3:
        return True
    return False


def click(event, y, x, flags, param):
    global point, images, shown
    # if left mouse is clicked, record coordinates (x,y)
    if event == cv2.EVENT_LBUTTONDOWN:
        point = (x, y)
        shown = False

def nothing(x):
    pass



for ii in range(2, 13):
    if ii != 9 and ii != 10:
        images.append(cv2.imread(path + str(ii) + '.tiff', cv2.IMREAD_LOAD_GDAL))


image_tci_initial = cv2.imread('/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv2.IMREAD_LOAD_GDAL)
# image_tci_initial[134, 225] = [0, 255, 0]

a = gdal.Open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff')
print a

xoffset, px_w, rot1, yoffset, px_h, rot2 = a.GetGeoTransform()

posX = px_w * 134 + rot1 * 225 + xoffset
posY = rot2 * 134 + px_h * 225 + yoffset

posX += px_w / 2.0
posY += px_h / 2.0

# get CRS from dataset
crs = osr.SpatialReference()
crs.ImportFromWkt(a.GetProjectionRef())
# create lat/long crs with WGS84 datum
crsGeo = osr.SpatialReference()
crsGeo.ImportFromEPSG(4326) # 4326 is the EPSG id of lat/long crs
t = osr.CoordinateTransformation(crs, crsGeo)
(lat, long, z) = t.TransformPoint(posX, posY)

print"lat + long"
print (lat)
print long

pedo = cv2.imread( '/home/razvan/an4/maps_licenta/cropped_raw.tif' , cv2.IMREAD_LOAD_GDAL)
# pedo[1, 1] = [0, 255, 0, 100]
cv2.imshow('kmz', pedo)

cv2.imshow('Iclod_Initial', image_tci_initial)

image_tci_after = image_tci_initial.copy()

cv2.namedWindow("Iclod_process")
cv2.setMouseCallback("Iclod_process", click)
# cv2.createTrackbar("epsilon", "Iclod_process", 0, 50, nothing)
# cv2.createTrackbar("color_dif", "Iclod_process", 0, 50, nothing)
cv2.imshow("Iclod_process", image_tci_after)


# keep looping until q key is pressed
while True:
    cv2.imshow("Iclod_process", image_tci_after)
    cv2.imshow("pedo_proc", pedo)
    key = cv2.waitKey(500) & 0xFF

    if key == ord("r"):
        image_tci_after = image_tci_initial.copy()
        pedo = pedo.copy()

    if key == ord("q"):
        break
    if key == ord("s"):
        write_to_excel()
        cv2.imwrite("/home/razvan/an4/try.tiff", image_tci_after)

    if key == ord("c"):
        image_tci_after = image_tci_initial.copy()
        # process_image(point[0], point[1], epsilonn, 0)
        process_image_static(point[0], point[1])
    # epsilonn = cv2.getTrackbarPos("epsilon", "Iclod_process")
    # color_difff = cv2.getTrackbarPos("color_dif", "Iclod_process")

    # if point[0] != -1:
    #     image_tci_after = image_tci_initial.copy()
    #     # process_image(point[0], point[1], epsilonn, 0)
    #     process_image_static(point[0], point[1])


cv2.waitKey(0)
cv2.destroyAllWindows()