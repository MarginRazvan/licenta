import matplotlib.pyplot as plt
from matplotlib import style
from sklearn.cluster import KMeans
import numpy as np
import math
from kneed import KneeLocator
from sklearn.datasets.samples_generator import make_blobs

def get_k_means_all_features_percentage(points, flag=False, max_clusters=11):
    x, y, distance = zip(*points)

    x = list(x)
    y = list(y)
    distance = list(distance)

    cost = []
    labels = []
    centroids = []

    for i in range(1, max_clusters):
        KM = KMeans(n_clusters=i, max_iter=2500, n_init=15, random_state=42)
        KM.fit(np.array(distance))
        labels.append(KM.labels_)
        centroids.append(KM.cluster_centers_)
        cost.append(KM.inertia_)

    index = range (1, len(cost) + 1)

    kn = KneeLocator(index, cost, curve='convex', direction='decreasing')
    knn = kn.knee
    # central_point = get_best_centroid_all_features(zip(x, y, distance, labels[kn.knee-1]), centroids[kn.knee-1])
    main_cluster, central_point = get_best_centroid_all_features(zip(x, y, distance, labels[knn-1]), centroids[knn-1])
    percentage = 100.0 * len(main_cluster) / len(distance)

    # print zip(x, y, distance, labels[kn.knee])
    # return zip(x, y, distance, labels[kn.knee-1]), kn.knee, central_point
    return zip(x, y, distance, labels[knn - 1]), knn, central_point, main_cluster, percentage

def get_k_means_all_features(points, flag=False, max_clusters=11):
    x, y, distance = zip(*points)

    x = list(x)
    y = list(y)
    distance = list(distance)

    cost = []
    labels = []
    centroids = []
    delta_cost = []
    k = 1
    percentage = 100.0
    KM = KMeans(n_clusters=1, max_iter=2500, n_init=15, random_state=42)
    KM.fit(np.array(distance))
    labels.append(KM.labels_)
    centroids.append(KM.cluster_centers_)
    cost.append(KM.inertia_)
    while percentage > 30.0 and k < len(distance):
        # print percentage
        k += 1
        KM = KMeans(n_clusters=k, max_iter=2500, n_init=15, random_state=42)
        KM.fit(np.array(distance))
        labels.append(KM.labels_)
        centroids.append(KM.cluster_centers_)
        cost.append(KM.inertia_)
        delta = abs(cost[k - 2] - cost[k - 1])
        percentage = (delta * 100.0) / cost[k - 2]
    knn = k - 1
    print knn


    # if max_clusters != 11:
    #     k = 1
    #     percentage = 100.0
    #     KM = KMeans(n_clusters=1, max_iter=2500, n_init=15, random_state=42)
    #     KM.fit(np.array(distance))
    #     labels.append(KM.labels_)
    #     centroids.append(KM.cluster_centers_)
    #     cost.append(KM.inertia_)
    #     while percentage > 5.0:
    #         k += 1
    #         KM = KMeans(n_clusters=k, max_iter=2500, n_init=15, random_state=42)
    #         KM.fit(np.array(distance))
    #         labels.append(KM.labels_)
    #         centroids.append(KM.cluster_centers_)
    #         cost.append(KM.inertia_)
    #         delta = cost[k-2] - cost[k-1]
    #         percentage = (delta * 100.0) / cost[k-2]
    #     knn = k - 2
    #     print knn
    #     print cost[knn - 1]
    # else:
    # max_c = 11
    # if len(distance) < 11:
    #     max_c = 4
    #
    # for i in range(1, max_c):
    #     KM = KMeans(n_clusters=i, max_iter=2500, n_init=15, random_state=42)
    #     KM.fit(np.array(distance))
    #     # print KM.labels_
    #     labels.append(KM.labels_)
    #     centroids.append(KM.cluster_centers_)
    #     # calculates squared error
    #     # for the clustered points
    #     cost.append(KM.inertia_)
    # # print "cost"
    # # print cost
    # #
    # index = range (1, len(cost) + 1)
    # #
    # kn = KneeLocator(index, cost, curve='convex', direction='decreasing')
    # knn = kn.knee
    # print kn.knee
    # print kn.elbow
    # print knn


    # plot the cost against K values
    # if flag is True:
    #     lista = []
    #     index = 1
    #     for el in cost:
    #         lista.append((index, cost))
    #         index += 1
    #     print "asdf"
    #     print lista
    #     print "Asdf"
    #
    # plt.plot(range(1, max_c), cost, color='g', linewidth='3')
    # plt.xlabel("Value of K")
    # plt.ylabel("Squared Error (Cost)")
    # plt.vlines(kn.knee, plt.ylim()[0], plt.ylim()[1], linestyles='dashed')
    # plt.show()

    # print labels[kn.knee-1]
    # print centroids[kn.knee-1]
    # central_point = get_best_centroid_all_features(zip(x, y, distance, labels[kn.knee-1]), centroids[kn.knee-1])
    main_cluster, central_point = get_best_centroid_all_features(zip(x, y, distance, labels[knn-1]), centroids[knn-1])
    # print zip(x, y, distance, labels[kn.knee])
    # return zip(x, y, distance, labels[kn.knee-1]), kn.knee, central_point
    return zip(x, y, distance, labels[knn - 1]), knn, central_point, main_cluster

def get_best_centroid_all_features(dist, centroids):
    best_centroid = (0, 0, 0, 0)
    count = [0] * len(centroids)
    separated = [[] for i in range(len(centroids))]
    for element in dist:
        count[element[3]] += 1
        separated[element[3]].append(element)

    max = 0
    index = 0
    # print count
    for i in range(len(count)):
        # print str(i) + str(count[i])
        if count[i] > max:
            max = count[i]
            index = i
            # best_centroid = separated[i][int(len(separated[i])/2)]

    # print index

    best_centroid2 = (0, 0, 0, 0)


    min = float('inf')
    for element in separated[index]:
        sp_dist = 0
        indexxx = 0
        for band_value in element[2]:
            sp_dist += math.pow(band_value - centroids[index][indexxx], 2)
            indexxx += 1
        sp_dist = math.sqrt(sp_dist)
        # print sp_dist
        if sp_dist < min:
            min = sp_dist
            best_ever_fucking_element = element
    # print best_ever_fucking_element


    # return (best_ever_fucking_element[0], best_ever_fucking_element[1])
    return separated[index], best_ever_fucking_element
    # print count

def get_k_means(points):
    x, y, distance = zip(*points)
    x = list(x)
    y = list(y)
    distance = list(distance)
    cost = []
    labels = []
    centroids = []
    for i in range(1, 11):
        KM = KMeans(n_clusters=i, max_iter=2500, n_init=15, random_state=42)
        KM.fit(np.array(distance).reshape(-1,1))
        # print KM.labels_
        labels.append(KM.labels_)
        centroids.append(KM.cluster_centers_)
        # calculates squared error
        # for the clustered points
        cost.append(KM.inertia_)
    # print "cost"
    # print cost

    index = range (1, len(cost) + 1)

    kn = KneeLocator(index, cost, curve='convex', direction='decreasing')
    # print kn.knee
    # print kn.elbow



    # plot the cost against K values
    plt.plot(range(1, 11), cost, color='g', linewidth='3')
    plt.xlabel("Value of K")
    plt.ylabel("Squared Error (Cost)")
    plt.vlines(kn.knee, plt.ylim()[0], plt.ylim()[1], linestyles='dashed')
    plt.show()

    central_point =  get_best_centroid(zip(x,y,distance, labels[kn.knee-1]), centroids[kn.knee-1])

    # print zip(x, y, distance, labels[kn.knee])
    return zip(x, y, distance, labels[kn.knee-1]), kn.knee, central_point


def get_best_centroid(dist, centroids):
    best_centroid = (0, 0, 0, 0)
    count = [0] * len(centroids)
    separated = [[] for i in range(len(centroids))]
    for element in dist:
        # print element
        count[element[3]] += 1
        separated[element[3]].append(element)
        # separated[element[3]] = insertt(separated[element[3]], element)

    for s in separated:
        s.sort(key= lambda tup:tup[2])

    max = 0
    index = 0
    # print count
    for i in range(len(count)):
        # print str(i) + str(count[i])
        if count[i] > max:
            max = count[i]
            index = i
            best_centroid = separated[i][int(len(separated[i])/2)]
    best_centroid2 = (0, 0, 0, 0)

    for iii in range(len(separated[index])):
        if iii != 0:
            if separated[index][iii][2] < separated[index][iii-1][2]:
                print "satu "

    for ii in range(len(separated[index])):
        if separated[index][ii][2] >= centroids[index]:
            value1 = separated[index][ii][2] - centroids[index]
            value2 = separated[index][ii-1][2] - centroids[index]

            if value1 >= value2:
                best_centroid2 = separated[index][ii]
                break
            else:
                best_centroid2 = separated[index][ii-1]
                break



    # print "#######"
    # print separated[index]
    # print best_centroid
    # print best_centroid2
    # print centroids
    # print "#######"
    return (best_centroid2[0], best_centroid2[1])
    # print count


def insertt(list, n):
    # Searching for the position
    index = 0
    for i in range(len(list)):
        if list[i][2] > n[2]:
            index = i
            break

    # Inserting n in the list
    # list = list[:i] + [n] + list[i:]
    list.insert(index, n)
    return list

# points = [(155, 204, 1.079968055083112), (155, 204, 0.7517659210153118), (155, 204, 0.7397323840416884), (155, 204, 0.586584179807127), (155, 204, 1.1650210298531096), (155, 204, 1.7469639378075326), (155, 204, 2.456932844015074), (155, 204, 2.3261465989915595), (155, 204, 1.9847397814323164), (155, 204, 1.9513093040315264), (155, 204, 0.9214857568079932), (155, 204, 0.7897594570500563), (155, 204, 0.4795977481181494), (155, 204, 0.1981792118260643), (155, 204, 0.2936732878557395), (155, 204, 0.6824126317705438), (155, 204, 1.542061607070223), (155, 204, 2.072159742876982), (155, 204, 1.8817691144239772), (155, 204, 1.9773196504359127), (155, 204, 0.8807809035168737), (155, 204, 0.7301499845922068), (155, 204, 0.426143168430517), (155, 204, 0.14962620091414483), (155, 204, 0.15538017891610237), (155, 204, 0.3350253721735116), (155, 204, 0.424330060212566), (155, 204, 1.2325570980688887), (155, 204, 1.350081849370622), (155, 204, 1.8503602351974602), (155, 204, 0.8451082770864333), (155, 204, 0.8486017911835916), (155, 204, 0.5526599316035131), (155, 204, 0.15561812233798475), (155, 204, 0.06888396039717827), (155, 204, 0.31732790611605527), (155, 204, 0.2121673867492364), (155, 204, 0.7603078323942216), (155, 204, 1.0588111257443416), (155, 204, 1.595358893791613), (155, 204, 0.959489968681278), (155, 204, 1.123453158792123), (155, 204, 0.8847259462681083), (155, 204, 0.10286398786747487), (155, 204, 0.09732420048477157), (155, 204, 0.33507312634707065), (155, 204, 0.12541929676090516), (155, 204, 0.5663744344512734), (155, 204, 0.8138040304643371), (155, 204, 1.2499027962205702), (155, 204, 1.1617697706516554), (155, 204, 1.3864959430160622), (155, 204, 1.4663243161047284), (155, 204, 0.48937000316733753), (155, 204, 0.2761159176867567), (155, 204, 0.0), (155, 204, 0.3092878917772241), (155, 204, 0.6218408156433605), (155, 204, 0.8401874790783305), (155, 204, 1.0001859827052164), (155, 204, 0.7384402480905276), (155, 204, 1.1967694013468093), (155, 204, 1.4659450876482378), (155, 204, 0.6893446162841922), (155, 204, 0.2183918496647712), (155, 204, 0.19670282153543178), (155, 204, 0.2663362536343858), (155, 204, 0.5659885157845517), (155, 204, 0.9224207283013537), (155, 204, 0.8596993660576934), (155, 204, 0.46848479164216195), (155, 204, 0.938233446430045), (155, 204, 1.4889993284081764), (155, 204, 0.7349251662584431), (155, 204, 0.3019950330717375), (155, 204, 0.23726988852359665), (155, 204, 0.168341913972724), (155, 204, 0.4254809043893745), (155, 204, 0.8720275225014403), (155, 204, 0.8885972090885723), (155, 204, 0.18932511719262166), (155, 204, 0.5230975052511719), (155, 204, 1.44564656814866), (155, 204, 0.658043311644454), (155, 204, 0.21135515134483945), (155, 204, 0.11980400661079742), (155, 204, 0.1778229456510042), (155, 204, 0.12574179893734602), (155, 204, 0.7309493826524516), (155, 204, 0.8581701462996715), (155, 204, 0.19224723665114152), (155, 204, 0.24733984717388316), (155, 204, 0.9744008415431507), (155, 204, 0.7318476617438903), (155, 204, 0.09903534722511946), (155, 204, 0.1447929556297544), (155, 204, 0.28336548837146697), (155, 204, 0.2546978602187307), (155, 204, 0.41642886547404456), (155, 204, 0.7454528824815153)]
#
# get_k_means(points)