import Cluster
import cv2
import transform_geo
import sys
import rasterio
import utm
sys.path.append('/usr/lib/python2.7/dist-packages/')


class Point:
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

class OpenCv:
    def __init__(self, latitude, longitude):
        path = '/home/razvan/an4/maps_licenta/cropped/B'
        self.images = []
        self.image = cv2.imread( '/home/razvan/an4/maps_licenta/cropped/TCI.tiff', cv2.IMREAD_LOAD_GDAL)
        for sp_band in range(2, 13):
            if sp_band != 9 and sp_band != 10:
                self.images.append(cv2.imread(path + str(sp_band) + '.tiff', cv2.IMREAD_LOAD_GDAL))
        x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/TCI.tiff", latitude, longitude)
        self.point = (x, y)

    def get_point(self):
        return self.point

    def get_clusters_elbow(self):
        x = self.point[0]
        y = self.point[1]

        cluster_points = []
        # follow box
        for i in range(x - 10, x + 10):
            for j in range(y - 10, y + 10):
                t = []
                self.image[i,j] = [0, 255, 255]
                for image in self.images:
                    t.append(image[i, j] / 1000.0)
                cluster_points.append((i, j, t))
        clusters, labels, central_point, main_cluster = Cluster.get_k_means_all_features(cluster_points)
        main_cluster_converted = []
        for element in main_cluster:
            with rasterio.open('/home/razvan/an4/maps_licenta/cropped/TCI.tiff') as map_layer:
                pixels2coords = map_layer.xy(element[0], element[1])
                lat_lon = utm.to_latlon(pixels2coords[0], pixels2coords[1], 34, 'T')
                data = {}
                data["latitude"] = lat_lon[0]
                data["longitude"] = lat_lon[1]
                main_cluster_converted.append(data)
        return main_cluster_converted


