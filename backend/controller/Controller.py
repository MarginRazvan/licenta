from flask import Flask, request, jsonify, send_file
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
from service.Service import Service
from dto.PointDTO import PointDTO
import transform_geo


app = Flask(__name__)
cors = CORS(app)
api = Api(app)

app.config['CORS_HEADERS'] = 'Contents-Type'


# spectral area
class SpecAreaEndpoint(Resource):
    @cross_origin()
    def post(self):
        reference_point = PointDTO(request.json["latitude"], request.json["longitude"])

        if request.json["length"] == "":
            request.json["length"] = 20

        if request.json["width"] == "":
            request.json["width"] = 20

        if request.json["threshold"] == "":
            request.json["threshold"] = 0.2

        points = service.pol_spec_area(reference_point, request.json["threshold"], request.json["length"], request.json["width"])
        return jsonify(points)


# seed propag
class SeedPropagEndpoint(Resource):
    @cross_origin()
    def post(self):
        if request.json["threshold"] == "":
            request.json["threshold"] = 0.2
        reference_point = PointDTO(request.json["latitude"], request.json["longitude"])
        points = service.seed_propag(reference_point, request.json["threshold"])
        return jsonify(points)


# local area
class LACEndpoint(Resource):
    @cross_origin()
    def post(self):
        reference_point = PointDTO(request.json["latitude"], request.json["longitude"])

        if request.json["length"] == "":
            request.json["length"] = 20

        if request.json["width"] == "":
            request.json["width"] = 20

        if request.json["percentageVariance"] == "":
            request.json["percentageVariance"] = 20.0

        points = service.lac(reference_point, request.json["percentageVariance"], request.json["length"], request.json["width"])
        return jsonify(points)


# extended local area
class ExtLocalAreaEndpoint(Resource):
    @cross_origin()
    def post(self):
        reference_point = PointDTO(request.json["latitude"], request.json["longitude"])

        if request.json["length"] == "":
            request.json["length"] = 20

        if request.json["width"] == "":
            request.json["width"] = 20

        if request.json["percentageVariance"] == "":
            request.json["percentageVariance"] = 20.0

        points = service.elac(reference_point, request.json["percentageVariance"], request.json["length"], request.json["width"])
        return jsonify(points)


class SaveEndpoint(Resource):
    @cross_origin()
    def get(self):
        pointDTO = PointDTO(float(request.args["lat"]), float(request.args["lon"]))
        x, y = transform_geo.get_coord_pixel("/home/razvan/an4/maps_licenta/cropped/TCI.tiff",
                                             pointDTO.latitude,
                                             pointDTO.longitude)
        return send_file("/home/razvan/an4/"+str(x)+"_"+str(y)+".xls", attachment_filename=request.args["lat"]+".xlsx", as_attachment=True)


api.add_resource(LACEndpoint, '/lac')
api.add_resource(ExtLocalAreaEndpoint, '/elac')
api.add_resource(SpecAreaEndpoint, '/specArea')
api.add_resource(SeedPropagEndpoint, '/seedPropag')
api.add_resource(SaveEndpoint, '/save')

if __name__ == '__main__':
    service = Service()
    app.run(port='5002')
