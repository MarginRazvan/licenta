export class PointSeedPropag {
    
    latitude: number;
    longitude: number;
    threshold: number;

  constructor(latitude: number, longitude: number, threshold: number) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.threshold = threshold;
  }
}

export class PointSpecArea {
    
  latitude: number;
  longitude: number;
  threshold: number;
  length: number;
  width: number;

  constructor(latitude: number, longitude: number, threshold: number, length: number, width: number) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.threshold = threshold;
    this.length = length;
    this.width = width;
  }
}

export class PointLAC {
    
  latitude: number;
  longitude: number;
  percentageVariance: number;
  length: number;
  width: number;

  constructor(latitude: number, longitude: number, percentageVariance: number, length: number, width: number) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.percentageVariance = percentageVariance;
    this.length = length;
    this.width = width;
  }
}