import { Component, OnInit } from '@angular/core';
import {MapModule, MapAPILoader, MarkerTypeId, IMapOptions, IBox, IMarkerIconInfo, WindowRef, 
  DocumentRef, MapServiceFactory, 
  BingMapAPILoaderConfig, BingMapAPILoader, 
  GoogleMapAPILoader,  GoogleMapAPILoaderConfig, ILatLong
} from 'angular-maps';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PointServiceService } from '../point-service/point-service.service';
import { PointSeedPropag, PointSpecArea, PointLAC } from '../models/point';
import {NgxSpinnerService} from 'ngx-spinner';
import { AlertPromise } from 'selenium-webdriver';
import { resolve } from 'url';
import { saveAs } from 'file-saver';
import { stringify } from 'querystring';

let PathData: Array<any> = null;

@Component({
  selector: 'app-maps-view',
  templateUrl: './maps-view.component.html',
  styleUrls: ['./maps-view.component.css']
})
export class MapsViewComponent implements OnInit {
  private myMap;
  loginForm: FormGroup;
  _markers: Array<ILatLong> = new Array<ILatLong>();
  _markers_reference: Array<ILatLong> = new Array<ILatLong>();
  private threshold;
  private index = 0;
  constructor(private pointService: PointServiceService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    // this.spinner.show();

    // setTimeout(() => {
    //   this.spinner.hide();
    // }, 5000);

    // console.log(this.spinner);
  }


  // ngAfterViewInit(): void { this.spinner.show(); }

  _markerTypeId = MarkerTypeId;
  _options: IMapOptions = {
    disableBirdseye: false,
    disableStreetside: false,
    navigationBarMode: 1, 
    zoom: 6
  };
  
  _box: IBox = {
    maxLatitude: 32,
    maxLongitude: -92,
    minLatitude: 29,
    minLongitude: -98
  };
  
  private _iconInfo: IMarkerIconInfo = {
    markerType: MarkerTypeId.FontMarker,
    fontName: 'FontAwesome',
    fontSize: 12,
    color: 'red',
    markerOffsetRatio: { x: 0.1, y: 0.1 },
    text: '\uF276'    
  };

  private _iconInfoGreen: IMarkerIconInfo = {
    markerType: MarkerTypeId.FontMarker,
    fontName: 'FontAwesome',
    fontSize: 12,
    color: 'green',
    markerOffsetRatio: { x: 0.1, y: 0.1 },
    text: '\uF276'    
  };
  
  _click(index: number){
     console.log(this._markers_reference[index])
    //  var filepath = String(this._markers_reference[index].latitude)+"_"+String(this._markers_reference[index].longitude);
    //  console.log(filepath);
    this.download(this._markers_reference[index].latitude, this._markers_reference[index].longitude);
  }
  
  _close(id: string){
    console.log(`infobox ${id} closed...`);
  }
  

  elac(percentageVariance, length, width) 
  {
    this.spinner.show();
    this._markers = [];
    var flag = false;
    var bar = new Promise((resolve, reject) => {
      this._markers_reference.forEach((element, index, array) => {
        var p = new PointLAC(element.latitude, element.longitude, percentageVariance, length, width);
        this.pointService.getPointsELac(p).subscribe((response) => {
          if (index === array.length -1) {
            flag = true;
          }
          if (flag === true) {
            response.forEach((element, index, array) => {
              this._markers.push({latitude: element.latitude, longitude: element.longitude});
              if (index === array.length -1) resolve();
            });
          } else {
            response.forEach(element => {
              this._markers.push({latitude: element.latitude, longitude: element.longitude});
            });
          }
        });
      });  
    });
  
    bar.then(() => {
        console.log('All done!');
        this.spinner.hide();
    });
    
  }

  lac(percentageVariance, length, width) 
  {
    this.spinner.show();
    this._markers = [];
    // var p = new Point(47.001105, 23.806555);
    var flag = false;
    var bar = new Promise((resolve, reject) => {
      this._markers_reference.forEach((element, index, array) => {
        var p = new PointLAC(element.latitude, element.longitude, percentageVariance, length, width);
        this.pointService.getPointsLac(p).subscribe((response) => {
          if (index === array.length -1) {
            flag = true;
          }
          if (flag === true) {
            response.forEach((element, index, array) => {
              this._markers.push({latitude: element.latitude, longitude: element.longitude});
              if (index === array.length -1) resolve();
            });
          } else {
            response.forEach(element => {
              this._markers.push({latitude: element.latitude, longitude: element.longitude});
            });
          }
        });
      });  
    });
  
    bar.then(() => {
        console.log('All done!');
        this.spinner.hide();
    });
  }


  specArea(threshold, length, width) 
  {
    this.spinner.show();
    this._markers = [];
    var flag = false;
    var bar = new Promise((resolve, reject) => {
      this._markers_reference.forEach((element, index, array) => {
        var p = new PointSpecArea(element.latitude, element.longitude, threshold, length, width);
        this.pointService.getPointsSpecArea(p).subscribe((response) => {
          if (index === array.length -1) {
            flag = true;
          }
          if (flag === true) {
            response.forEach((element, index, array) => {
              this._markers.push({latitude: element.latitude, longitude: element.longitude});
              if (index === array.length -1) resolve();
            });
          } else {
            response.forEach(element => {
              this._markers.push({latitude: element.latitude, longitude: element.longitude});
            });
          }
        });
      });  
    });
  
    bar.then(() => {
        console.log('All done!');
        this.spinner.hide();
    });
    
  }

  seedPropag(threshold) 
  {
    this.spinner.show();
    this._markers = [];
    var flag = false;
    var bar = new Promise((resolve, reject) => {
      this._markers_reference.forEach((element, index, array) => {
        var p = new PointSeedPropag(element.latitude, element.longitude, threshold);
        this.pointService.getPointsSeedPropag(p).subscribe((response) => {
          if (index === array.length -1) {
            flag = true;
          }
          if (flag === true) {
            response.forEach((element, index, array) => {
              this._markers.push({latitude: element.latitude, longitude: element.longitude});
              if (index === array.length -1) resolve();
            });
          } else {
            response.forEach(element => {
              this._markers.push({latitude: element.latitude, longitude: element.longitude});
            });
          }
        });
      });  
    });
  
    bar.then(() => {
        console.log('All done!');
        this.spinner.hide();
    });
    
  }

  _mapClicked(event) {
    console.log(event.location);
    this._markers_reference.push({latitude: event.location.latitude, longitude: event.location.longitude});
  }

  save() {
    this.pointService.getJSON();
    
  }

  download(latitude, longitude) {
    this.index += 1;
    var element = document.createElement("a");
    element.setAttribute('href', "http://127.0.0.1:5002/save?lat="+String(latitude)+"&lon="+String(longitude));
    element.setAttribute('download', "testing.xlsx");
   
    element.style.display = 'none';
    document.body.appendChild(element);
   
    element.click();
   
   document.body.removeChild(element);
   }

  
  


}
