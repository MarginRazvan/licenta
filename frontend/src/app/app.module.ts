import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { MapModule, MapAPILoader, BingMapAPILoaderConfig, BingMapAPILoader, WindowRef, DocumentRef, MapServiceFactory, BingMapServiceFactory } from "angular-maps";

import { AppComponent } from './app.component';
import { MapsViewComponent } from './maps-view/maps-view.component';
import {AppRoutingModule} from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import {NgxSpinnerModule} from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    MapsViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCxmA4c7cBVDd-3ZUZVYUBgpFSCjZXNK2k'
    }),
    MapModule.forRootBing(),
    HttpClientModule,
    NgxSpinnerModule
  ],
  providers: [
      { 
        provide: MapAPILoader, deps: [], useFactory:  BingMapServiceProviderFactory
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function BingMapServiceProviderFactory(){
  let bc: BingMapAPILoaderConfig = new BingMapAPILoaderConfig();
  bc.apiKey ="As069TGB_3rLkbeXUxhAjnaYhRVofoPgsxITfy9AGUCq8HPk_zQdTHslUA3cx2cc"; 
    // replace with your bing map key
    // the usage of this key outside this plunker is illegal. 
  bc.branch = "experimental"; 
    // to use the experimental bing brach. There are some bug fixes for
    // clustering in that branch you will need if you want to use 
    // clustering.
  return new BingMapAPILoader(bc, new WindowRef(), new DocumentRef());
}