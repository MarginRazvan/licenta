import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MapsViewComponent} from './maps-view/maps-view.component';

import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {path: '', component: MapsViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
