import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PointSeedPropag, PointSpecArea, PointLAC } from '../models/point';
import { map } from 'rxjs/operators';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';  

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';  
const EXCEL_EXTENSION = '.xlsx';  

@Injectable({
  providedIn: 'root'
})
export class PointServiceService {


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  httpOptions2 = {
    headers: new HttpHeaders({
      'Content-Type':  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    })
  };

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  constructor(private http: HttpClient) { }

  getPointsLac(point: PointLAC): Observable<any> {
    return this.http.post<any>('http://127.0.0.1:5002/lac', JSON.stringify(point), this.httpOptions)
    .pipe();
  }

  getPointsELac(point: PointLAC): Observable<any> {
    return this.http.post<any>('http://127.0.0.1:5002/elac', JSON.stringify(point), this.httpOptions)
    .pipe();
  }

  getPointsSpecArea(point: PointSpecArea): Observable<any> {
    return this.http.post<any>('http://127.0.0.1:5002/specArea', JSON.stringify(point), this.httpOptions)
    .pipe();
  }

  getPointsSeedPropag(point: PointSeedPropag): Observable<any> {
    return this.http.post<any>('http://127.0.0.1:5002/seedPropag', JSON.stringify(point), this.httpOptions)
    .pipe();
  }

  getJSON(): Observable<any> {  
    return this.http.get('http://127.0.0.1:5002/save?lat=0')
    .pipe();  
  }
  // return this.http.get<Blob>(endpoint, { reponseType: 'application/octet-stream'});
  public exportAsExcelFile(json: any[], excelFileName: string): void {  
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);  
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };  
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });  
    this.saveAsExcelFile(excelBuffer, excelFileName);  
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {  
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});  
    FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);  
 }  

  
}
